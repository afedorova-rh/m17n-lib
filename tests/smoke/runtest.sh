#!/bin/bash

# If one of the commands below returns non-zero then exit immediately
set -e

echo "Testing m17n-dump tool"
old_size=`stat -c "%s" old.png`
echo "old_size is $old_size"

m17n-dump hello.txt
new_size=`stat -c "%s" hello.txt.png`
echo "new_size is $new_size"
file old.png hello.txt.png
echo "Using font `fc-match`"

if [ $old_size -ne $new_size ]; then
        echo "m17n-dump generated different png file"
else
        echo "m17n-dump generated same png file"
fi
